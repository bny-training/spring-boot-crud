/**
 * 
 */
package com.customer.demo.response;

import java.util.List;

import com.customer.demo.model.Customer;

/**
 * @author shubham.kushwah
 *
 */
public class CustomerResponse {
private int code;
private String message;
private String status;
private Customer customer;
private List<Customer> customers;

/**
 * @return the customers
 */
public List<Customer> getCustomers() {
	return customers;
}
/**
 * @param customers the customers to set
 */
public void setCustomers(List<Customer> customers) {
	this.customers = customers;
}
/**
 * @return the code
 */
public int getCode() {
	return code;
}
/**
 * @param code the code to set
 */
public void setCode(int code) {
	this.code = code;
}
/**
 * @return the message
 */
public String getMessage() {
	return message;
}
/**
 * @param message the message to set
 */
public void setMessage(String message) {
	this.message = message;
}
/**
 * @return the status
 */
public String getStatus() {
	return status;
}
/**
 * @param status the status to set
 */
public void setStatus(String status) {
	this.status = status;
}
/**
 * @return the customer
 */
public Customer getCustomer() {
	return customer;
}
/**
 * @param customer the customer to set
 */
public void setCustomer(Customer customer) {
	this.customer = customer;
}

}
