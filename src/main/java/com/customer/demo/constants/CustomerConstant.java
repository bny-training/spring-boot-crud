/**
 * 
 */
package com.customer.demo.constants;

/**
 * @author shubham.kushwah
 *
 */
public class CustomerConstant {
	public static final String SUCCESS = "Success";
	public static final Integer TWO_HUNDRED = 200;
	public static final Integer NOT_FOUND = 404;
	public static final Integer FIVE_HUNDRED = 500;
	public static final String CUSTOMER_FOUND = "Customer Found";
	public static final String CUSTOMER_ADDED = "Customer Added";
	public static final String CUSTOMER_NOT_ADDED = "Customer Not Added";
	public static final String CUSTOMER_NOT_FOUND = "Customer Not Found";
	public static final String CUSTOMER_UPDATED = "Customer Updated";
	public static final String CUSTOMER_NOT_UPDATED = "Not Updated";
	public static final String FAILURE_DELETION = "Failure in Delete";
	public static final String FAILURE = "Failure";
	public static final String EXC_GET_CUSTOMER = "Exception while getting customer";
	public static final String EXC_GET_LST_CUSTOMER = "Exception while getting list of Customers";
	public static final String EXC_ADD_CUSTOMER = "Exception while adding Customer";
	public static final String EXC_UPT_CUSTOMER = "Exception while updating customer";
	public static final String EXC_RMV_CUSTOMER = "Exception while removing customer";

}
