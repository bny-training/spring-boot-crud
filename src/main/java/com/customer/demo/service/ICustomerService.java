/**
 * 
 */
package com.customer.demo.service;

import java.util.List;

import com.customer.demo.model.Customer;

/**
 * @author shubham.kushwah
 *
 */
public interface ICustomerService {

	public Customer getCustomer(int getId);

	public List<Customer> getAllCustomer();

	public Customer addCustomer(Customer cust);

	public Customer modifyCustomer(Customer emp);

	public String removeCustomer(int custId);

}
