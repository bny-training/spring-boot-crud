/**
 * 
 */
package com.customer.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.demo.constants.CustomerConstant;
import com.customer.demo.dao.CustomerDao;
import com.customer.demo.model.Customer;
import com.customer.demo.service.ICustomerService;

/**
 * @author shubham.kushwah
 *
 */
@Service
public class CustomerServiceImpl implements ICustomerService{

	@Autowired
	CustomerDao customerDao;
	
	@Override
	public Customer getCustomer(int getId) {
		Customer customer = new Customer();
		try {
			customer = convertToCustomer(customerDao.findById(getId));
			
		} catch (NoSuchElementException e) {
			customer = null;
			System.out.println(CustomerConstant.EXC_GET_CUSTOMER);
		}
		return customer;
	}

	private Customer convertToCustomer(Optional<Customer> findById) {
		Customer cust = null;
		if (findById != null) {
			 cust = new Customer(findById.get().getCustId(), findById.get().getCustName(), findById.get().getEmail(), findById.get().getNumber(), findById.get().getAddress());
		}
		return cust;
	}

	@Override
	public List<Customer> getAllCustomer() {
		List<Customer> custList = new ArrayList<>();
		try {
			custList = customerDao.findAll();
		} catch (Exception e) {
			System.out.println(CustomerConstant.EXC_GET_LST_CUSTOMER);
		}
		return custList;
	}

	@Override
	public Customer addCustomer(Customer cust) {
		Customer savedCust = new Customer();
		try {
			savedCust = customerDao.save(cust);
		} catch (Exception e) {
			savedCust= null;
			System.out.println(CustomerConstant.EXC_ADD_CUSTOMER);
		}
		return savedCust;
	}

	@Override
	public Customer modifyCustomer(Customer cust) {
		Customer customer = new Customer();
		try {
			customer = getCustomer(customer.getCustId());

			if (customer != null) {
				customer.setAddress(cust.getAddress());;
				customer.setCustName(cust.getCustName());;
				customer.setEmail(cust.getEmail());;
				customer.setNumber(cust.getNumber());;

				customerDao.save(customer);
			}
			
		} catch (Exception e) {
			customer = null;
			System.out.println(CustomerConstant.EXC_UPT_CUSTOMER);
		}
		return customer;
	}

	@Override
	public String removeCustomer(int custId) {
		Customer customer = new Customer();
		boolean flag = false;
		try {
			customer = getCustomer(custId);
			if (customer != null) {
				customerDao.deleteById(custId);
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(CustomerConstant.EXC_RMV_CUSTOMER);
			return null;
		}
		if (flag) {
			return "customer deleted Successfully with id= " + custId;
		} else {
			return "id " + custId + " does not exist";
		}
	}

}
