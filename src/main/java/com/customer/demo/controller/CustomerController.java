/**
 * 
 */
package com.customer.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.customer.demo.constants.CustomerConstant;
import com.customer.demo.model.Customer;
import com.customer.demo.response.CustomerResponse;
import com.customer.demo.service.ICustomerService;

/**
 * @author shubham.kushwah
 *
 */
@RestController
public class CustomerController {
	@Autowired
	ICustomerService iCustomerService;
	
	@GetMapping("/allCustomers")
	public CustomerResponse getAllCustomer(){
		CustomerResponse customerResponse = new CustomerResponse();
		List<Customer> getAllCustomer = iCustomerService.getAllCustomer();

		if(getAllCustomer!=null && !getAllCustomer.isEmpty()) {
			customerResponse.setCode(CustomerConstant.TWO_HUNDRED);
			customerResponse.setCustomers(getAllCustomer);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_FOUND);
			customerResponse.setStatus(CustomerConstant.SUCCESS);
		}else {
			customerResponse.setCode(CustomerConstant.NOT_FOUND);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_NOT_FOUND);
			customerResponse.setStatus(CustomerConstant.FAILURE);
		}
		return customerResponse;
	}
	
	@GetMapping("/getCustomerById/{getId}")
	public CustomerResponse getCustomer(@PathVariable("getId") int getId) {
		CustomerResponse customerResponse = new CustomerResponse();
		Customer customer = iCustomerService.getCustomer(getId);
		if(customer!=null) {
			customerResponse.setCode(CustomerConstant.TWO_HUNDRED);
			customerResponse.setCustomer(customer);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_FOUND);
			customerResponse.setStatus(CustomerConstant.SUCCESS);
		}else {
			customerResponse.setCode(CustomerConstant.NOT_FOUND);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_NOT_FOUND);
			customerResponse.setStatus(CustomerConstant.FAILURE);
		}
		return customerResponse;
	}
	
	
	@PostMapping("/addCustomer")
	public CustomerResponse addCustomer(@RequestBody Customer cust) {
		CustomerResponse customerResponse = new CustomerResponse();

		Customer addCustomer = iCustomerService.addCustomer(cust);
		if(addCustomer!=null ) {
			customerResponse.setCode(CustomerConstant.TWO_HUNDRED);
			customerResponse.setCustomer(addCustomer);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_ADDED);
			customerResponse.setStatus(CustomerConstant.SUCCESS);
		}else {
			customerResponse.setCode(CustomerConstant.FIVE_HUNDRED);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_NOT_FOUND);
			customerResponse.setStatus(CustomerConstant.FAILURE);
		}
		return customerResponse;
	}
	@PutMapping("/updateCustomer/{custId}")
	public CustomerResponse modifyCustomer(@RequestBody Customer cust) {
		CustomerResponse customerResponse = new CustomerResponse();

		Customer modifyCustomer = iCustomerService.modifyCustomer(cust);
		if(modifyCustomer!=null ) {
			customerResponse.setCode(CustomerConstant.TWO_HUNDRED);
			customerResponse.setCustomer(modifyCustomer);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_UPDATED);
			customerResponse.setStatus(CustomerConstant.SUCCESS);
		}else {
			customerResponse.setCode(CustomerConstant.FIVE_HUNDRED);
			customerResponse.setMessage(CustomerConstant.CUSTOMER_NOT_UPDATED);
			customerResponse.setStatus(CustomerConstant.FAILURE);
		}
		return customerResponse; 
	}
	
	@DeleteMapping("/deleteCustomer/{custId}")
	public CustomerResponse removeCustomer(@PathVariable("custId") int custId) {
		String removeCustomer = iCustomerService.removeCustomer(custId);
		CustomerResponse customerResponse = new CustomerResponse();
		if(removeCustomer!=null) {	
		customerResponse.setCode(CustomerConstant.TWO_HUNDRED);
			customerResponse.setMessage(removeCustomer);
			customerResponse.setStatus(CustomerConstant.SUCCESS);
		}else {
			customerResponse.setCode(CustomerConstant.FIVE_HUNDRED);
			customerResponse.setMessage(CustomerConstant.FAILURE_DELETION);
			customerResponse.setStatus(CustomerConstant.FAILURE);
		}
		return customerResponse; 
	}
}
