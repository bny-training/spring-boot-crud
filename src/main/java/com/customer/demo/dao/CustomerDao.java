/**
 * 
 */
package com.customer.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.customer.demo.model.Customer;


/**
 * @author shubham.kushwah
 *
 */
@Repository
public interface CustomerDao extends JpaRepository<Customer, Integer>{

}
